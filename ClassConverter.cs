﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Globalization;


//name of group in UiPath
namespace Diacritics_Converter
{
    //activity name in uipath
    public class Convert_Diacritics : CodeActivity
    {
        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> Input { get; set; }

        [Category("Output")]
        public OutArgument<string> Output { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            string input = Input.Get(context);
            string output = Output.Get(context);
            byte[] bytes;

            input = input.Replace("ß", "ss");
            input = input.Replace("ẞ", "SS");
            input = input.Replace("ð", "o");
            input = input.Replace("â", "a");

            bytes = Encoding.GetEncoding("ISO-8859-8").GetBytes(input);
            output = Encoding.UTF8.GetString(bytes);

            Output.Set(context, output);
        }
    }

    public class Contains_Diacritics : CodeActivity
    {
        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> Input { get; set; }

        [Category("Result")]
        public OutArgument<bool> Result { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            string input = Input.Get(context);
            string converted;
            bool result = Result.Get(context);
            byte[] bytes;

            input = Regex.Replace(input, "ß", "ss");
            input = Regex.Replace(input, "ð", "o");

            bytes = Encoding.GetEncoding("ISO-8859-8").GetBytes(input);
            converted = Encoding.UTF8.GetString(bytes);
            if (input == converted)
            {
                result = false;
            }
            else
            {
                result = true;
            }

            Result.Set(context, result);
        }
    }

    //This converts accented characters to nonaccented, which means it is
    /// easier to search for matching data with or without such accents.
    public class Convert_Accents : CodeActivity
    {
        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> Input { get; set; }

        [Category("Output")]
        public OutArgument<string> Output { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            string input = Input.Get(context);
            string output = Output.Get(context);

            output = string.Concat(input.Normalize(NormalizationForm.FormD).Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)).Normalize(NormalizationForm.FormC);

            Output.Set(context, output);
        }
    }

    //reutrns true if provided string contain accents
    public class Contains_Accents : CodeActivity
    {
        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> Input { get; set; }

        [Category("Result")]
        public OutArgument<bool> Result { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            string input = Input.Get(context);
            bool result = Result.Get(context);

            foreach (char c in input)
            {
                var s = c.ToString().Normalize(NormalizationForm.FormD);
                result = (s.Length > 1) &&
                       char.IsLetter(s[0]) &&
                       s.Skip(1).All(c2 => CharUnicodeInfo.GetUnicodeCategory(c2) == UnicodeCategory.NonSpacingMark);
                if (result == true)
                {
                    break;
                }
            }

            Result.Set(context, result);
        }

    }

}