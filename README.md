# UIPath Diacritics Converter

Simple set of activities to operate on strings with diacritics or accents.

Contains four activities: 
* 1. Convert diacritics in provided string to non-diacritic string. 
* 2. Return false/true if provided string not/contains diacritics. 
* 3. Convert accented characters to non-accented. 
* 4. Return false/true if provided string not/contains accented letters. 

## Authors

* **p0tfur** 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
